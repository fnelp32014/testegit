package org.fundacaonokia.org;

public class Pessoa {

    private int idade;
    private String nome;
    private String rg;
    private String cpf;

    public Pessoa(int idade, String nome, String rg, String cpf) {
        super();
        this.idade = idade;
        this.nome = nome;
        this.rg = rg;
        this.cpf = cpf;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

}
